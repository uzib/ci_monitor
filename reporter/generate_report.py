import os
from jinja2 import Environment, PackageLoader
from monitor.utils import write_result_file, print_result_data


def generate_report(template_name, report_data, report_output, save_to_file=None, print_data=False):

    file_loader = PackageLoader('reporter', 'templates')
    env = Environment(loader=file_loader, trim_blocks=True)
    template_path = os.path.join(template_name)
    template_env = env.get_template(template_path)

    if print_data:
        print_result_data(report_data)

    if save_to_file:
        write_result_file(result_file=save_to_file, data=report_data)

    with open(report_output, 'w') as f:
        f.write(template_env.render(data=report_data))
