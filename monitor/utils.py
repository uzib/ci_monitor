import logging
import yaml
import urllib3
import jenkins
from datetime import datetime


class ManageJenkins(object):

    def __init__(self, username, token, server_hostname='cp-jenkins.argus-sec.com', port='8443'):
        self.server_url = "https://{}:{}@{}:{}".format(username, token, server_hostname, port)
        self.username = username
        self.con = None
        self.date_frmt = '%Y-%m-%d %H:%M:%S'

    def connect(self):
        # TODO: disable ssl, or find a way to get and use jenkins cert.
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        logging.debug("connecting to '{}' as '{}'".format(self.server_url, self.username))
        self.con = jenkins.Jenkins(self.server_url)
        self.con._session.verify = False
        res = self.__check_connection()
        if not res:
            raise KeyError("Failed to connect")
        logging.info("connected to jenkins")

    def __check_connection(self):

        user = self.con.get_whoami()['id']
        logging.debug(user)
        return user == self.username

    def convert_jenkins_timestamp(self, timestamp):
        timestamp = timestamp / 1000
        return datetime.utcfromtimestamp(timestamp).strftime(self.date_frmt)


def check_unit_tests(tests):
    """
    Check the Unit test stage of a list of .....
    :param tests:
    :return:
    """
    tests_status = []
    tests_failed = []
    for test in tests:
        tests_status.append(test['status'] == 'PASSED')
        if test['status'] != 'PASSED':
            tests_failed.append((test['name'], test['className'], test['errorDetails'], test['errorStackTrace']))
            msg = "The test {} from suite {} Failed with:\nerrorDetails =\n {}\n errorStackTrace =\n {}"
            msg = msg.format(test['name'], test['className'], test['errorDetails'], test['errorStackTrace'])
            print msg
    return all(tests_status), tests_failed


def check_automation_tests(tests):
    tests_status = []
    tests_failed = []
    for test in tests:
        status = test['status']
        if status not in ['FIXED', 'SKIPPED']:
            # TODO: 'FIXED', 'SKIPPED' - 'skippedMessage' (84894368),
            tests_status.append(status == 'PASSED')
        if status not in ['PASSED', 'FIXED', 'SKIPPED']:
            tests_failed.append({'name': str(test['name']), 'className': str(test['className']),
                                 'errorDetails': str(test['errorDetails'].encode('ascii', 'ignore')),
                                 'errorStackTrace': str(test['errorStackTrace'].encode('ascii', 'ignore'))})
            msg = "The test {} from suite {} Failed with:\nerrorDetails =\n {}\n errorStackTrace =\n {}"
            msg = msg.format(test['name'], test['className'], test['errorDetails'].encode('ascii', 'ignore'),
                             test['errorStackTrace'].encode('ascii', 'ignore'))
            print msg
    return all(tests_status), tests_failed


def check_triggered_builds(jm_obj, job_name, build_number):
    # TODO: use regex, not loop
    console_output = jm_obj.con.get_build_console_output(job_name, build_number).split('\n')
    triggered_builds = {}
    for line in console_output:
        if 'Starting building: ' in line:
            res = line.replace('Starting building: ', '').split(' ')
            triggered_job_name, triggered_build_number = res[0], int(res[1].replace('#', ''))
            build_info = jm_obj.con.get_build_info(triggered_job_name, triggered_build_number)
            triggered_job_info = {'build_number': triggered_build_number, 'url': str(build_info['url']),
                                  'result': str(build_info['result'])}
            triggered_builds.update({triggered_job_name: triggered_job_info})

    return triggered_builds


def get_commit_info(build_info):
    commits_info = []
    actions = build_info[u'actions']
    for action in actions:
        if action != {}:
            jenkins_class = action[u'_class']
            if jenkins_class == u'hudson.plugins.git.util.BuildData':
                repo = str(action[u'remoteUrls'][0])
                commit = str(action[u'lastBuiltRevision'][u'SHA1'])
                branch = str(action[u'lastBuiltRevision'][u'branch'][0][u'name'])
                commits_info.append({'commit-id': commit, 'commit-url': repo + '/commits/' + commit, 'branch': branch})
    return commits_info


def check_build(jm_obj, job_name, build_number, to_check_unit_tests=True, to_check_automation_tests=True,
                to_check_assertion_error=True, to_check_triggered_builds=True):

    build_info = jm_obj.con.get_build_info(job_name, build_number)
    build_date = convert_jenkins_timestamp(build_info['timestamp'])
    url = str(build_info['url'])
    result = str(build_info['result'])
    commits_info = get_commit_info(build_info)

    build_report_stages_data = {'Unit Tests': 'NA', 'Automation Tests': 'NA', 'Assertion Error': 'NA',
                                'Triggered Builds': 'NA'}

    build_report_info = {'job_name': job_name, 'build_number': build_number, 'Date': build_date, 'Result': result,
                         'url': url, 'commits': commits_info, 'stages': build_report_stages_data}

    print "Checking '{}' build '{}'\n url: {}".format(job_name, build_number, build_info['url'])

    if build_info['result'] == 'SUCCESS':
        build_report_info.update(
            {'Unit Tests': 'SUCCESS', 'Automation Tests': 'SUCCESS', 'Assertion Error': 'SUCCESS',
             'Triggered Builds': 'SUCCESS'})
        return build_report_info

    unit_tests_performed = False
    unit_tests_passed = 'NA'
    unittests_data = 'NR'

    automation_tests_performed = False
    automation_tests_passed = 'NA'
    automation_tests_data = 'NR'

    assertion_tests_performed = False
    assertion_tests_passed = 'NA'
    assertion_error_data = 'NR'

    unittests_info = {}
    automation_tests_info = {}
    if to_check_unit_tests or to_check_automation_tests:
        build_test_report = jm_obj.con.get_build_test_report(job_name, build_number, 3)
        suites = []
        try:
            suites = build_test_report['suites']
        except TypeError as err:
            print("Unit tests and automation tests were not performed. Got '{}'".format(err))
        finally:
            for suite in suites:
                if 'Unit Tests' in suite['enclosingBlockNames'] and to_check_unit_tests:
                    unit_tests_performed = True
                    unit_tests_passed, unittests_data = check_unit_tests(tests=suite['cases'])

                elif 'Automation Tests' in suite['enclosingBlockNames'] and to_check_automation_tests:
                    automation_tests_performed = True
                    automation_tests_passed, failed_tests = check_automation_tests(tests=suite['cases'])
                    test_result = '{}.{}\n Got: {}'
                    automation_tests_data = [test_result.format(test['className'], test['name'], test['errorDetails'])
                                             for test in failed_tests]

        unittests_info = {'performed': unit_tests_performed, 'passed': unit_tests_passed, 'data': unittests_data}
        automation_tests_info = {'performed': automation_tests_performed, 'passed': automation_tests_passed,
                                 'data': automation_tests_data}

    # Check assertion error -
    assertion_error_info = {}
    if to_check_assertion_error:
        assertion_error_info = {'performed': False, 'passed': 'NA', 'data': None}
        console_output = jm_obj.con.get_build_console_output(job_name, build_number)
        assertion_tests_performed = 'python verify_assertion_errors.py' in console_output
        if assertion_tests_performed:
            assertion_tests_passed = 'Found problematic assertion errors in file' not in console_output
            if not assertion_tests_passed:
                assertion_error_data = str(console_output[console_output.find(
                    'Found problematic assertion errors in file'): console_output.find(
                    'raise FoundArgusAssertionErrors()')]).split('\n')
    assertion_error_info.update(
        {'performed': assertion_tests_performed, 'passed': assertion_tests_passed, 'data': assertion_error_data})

    triggered_builds_info = {}
    if to_check_triggered_builds:

        triggered_builds = check_triggered_builds(jm_obj, job_name, build_number)
        for triggered_job_name, triggered_build_info in triggered_builds.items():
            triggered_builds_info.update({str(triggered_job_name): {'performed': True,
                                                                    'data': triggered_build_info['url'],
                                                                    'passed': triggered_build_info[
                                                                                  'result'] == 'SUCCESS'}})

    build_report_stages_data.update({'Unit Tests': unittests_info, 'Automation Tests': automation_tests_info,
                                     'Assertion Error': assertion_error_info,
                                     'Triggered Builds': triggered_builds_info})

    build_report_info.update({'stages': build_report_stages_data})
    return build_report_info


def check_last_build_of_monitored_jobs(jm_obj, jobs_to_check):

    data = {}
    for job_name in jobs_to_check:
        job_info = jm_obj.con.get_job_info(name=job_name, depth=1)
        last_build = int(job_info[u'lastCompletedBuild'][u'id'])
        build_info = check_build(jm_obj, job_name, last_build)
        data.update({job_name+'-'+str(last_build): build_info})
    return data


def convert_jenkins_timestamp(timestamp):
    timestamp = timestamp/1000
    return datetime.utcfromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S')


def read_result_file(result_file):
    with open(result_file, 'r') as f:
        data = yaml.safe_load(f.read())
    return data


def write_result_file(result_file, data):
    with open(result_file, 'w') as f:
        f.write(yaml.dump(data, allow_unicode=False, default_flow_style=False))


def print_result_data(data):
    print(yaml.dump(data, allow_unicode=True, default_flow_style=False))
