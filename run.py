import os
from datetime import datetime
from reporter.generate_report import generate_report
from monitor.utils import ManageJenkins, check_build, check_last_build_of_monitored_jobs, read_result_file

VERSION_3_3_X = ['3.3.x_Default_Full_3hr', '3.3.x_NAD_Full_3hr', '3.3.x_SOC_Full_3hr', '3.3.x_SOC_Sanity_1hr',
                 '3.3.x_GM_Full_3hr']
VERSION_GA = ['Master_Default_Sanity_1hr', 'Master_Default_Full_3hr', 'CP_MASTER_Random_Seed_UTs']
RSPI_YOCTO = ['yocto_package_handler', 'kernel_package_handler', 'yocto_package_handler_3.3.x']
JOBS_TO_MONITOR = VERSION_3_3_X + VERSION_GA + RSPI_YOCTO


def daily_report(jm_obj, read_from_file=''):

    template_name = 'daily_ci_report_template.html'
    folder_path = os.path.join('reporter', 'output', datetime.now().strftime('%Y_%m_%d-%H_%M_%S'))
    report_file = os.path.join(folder_path, 'daily_ci_report_output.html')
    save_to_file = os.path.join(folder_path, 'daily_ci_report_output_result.yaml')
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)

    if read_from_file:
        data = read_result_file(read_from_file)
    else:
        data = check_last_build_of_monitored_jobs(jm_obj=jm_obj, jobs_to_check=JOBS_TO_MONITOR)
    generate_report(template_name=template_name, report_output=report_file, report_data=data, save_to_file=save_to_file,
                    print_data=True)


def failed_build_report(jm_obj, job_name, build_number, read_from_file=''):

    template_name = 'failed_build_ci_report_template.html'
    folder_path = os.path.join('reporter', 'output', datetime.now().strftime('%Y_%m_%d-%H_%M_%S'))
    report_file = os.path.join(folder_path, 'failed_build_ci_report_output.html')
    save_to_file = os.path.join(folder_path, 'failed_build_ci_report_result.yaml')
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)

    if os.path.isfile(read_from_file):
        data = read_result_file(read_from_file)
    else:
        data = check_build(jm_obj=jm_obj, job_name=job_name, build_number=build_number)
    generate_report(template_name=template_name, report_output=report_file, report_data=data, save_to_file=save_to_file,
                    print_data=True)


def daily_report_with_failed_build(jm_obj, read_from_file=''):
    folder_path = os.path.join('reporter', 'output', datetime.now().strftime('%Y_%m_%d-%H_%M_%S'))
    report_file = os.path.join(folder_path, 'daily_report_with_failed_build_output.html')
    save_to_file = os.path.join(folder_path, 'daily_report_with_failed_build_result.yaml')
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)

    if os.path.isfile(read_from_file):
        data = read_result_file(read_from_file)
    else:
        data = check_last_build_of_monitored_jobs(jm_obj=jm_obj, jobs_to_check=JOBS_TO_MONITOR)
    generate_report(template_name='daily_ci_report_template.html', report_output=report_file, report_data=data,
                    save_to_file=save_to_file, print_data=True)

    for build, build_info in data.items():
        if build_info['Result'] != 'SUCCESS':
            failed_report_file = os.path.join(folder_path, '{}.html'.format(build))
            generate_report(template_name='failed_build_ci_report_template.html', report_output=failed_report_file,
                            report_data=build_info, print_data=True)


if __name__ == '__main__':

    with open('data/credentials_uzi.txt') as f:
        res = f.read().split()
    jm = ManageJenkins(username=res[0], token=res[1])
    jm.connect()

    # daily_report(jm)
    # daily_report(jm, read_from_file=r'data\daily_ci_report_output_result.yaml')
    # failed_build_report(jm, '3.3.x_Default_Full_3hr', 331)
    # failed_build_report(jm, '3.3.x_Default_Full_3hr', 331, read_from_file=r'data\failed_build_ci_report_result.yaml')
    daily_report_with_failed_build(jm)
    # daily_report_with_failed_build(jm, read_from_file=r'data\daily_ci_report_output_result.yaml')
